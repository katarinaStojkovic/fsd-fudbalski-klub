package com.katarina.stojkovic.fsd.service.impl;


import com.katarina.stojkovic.fsd.entity.Klub;
import com.katarina.stojkovic.fsd.entity.Mesto;
import com.katarina.stojkovic.fsd.repository.KlubRepository;
import com.katarina.stojkovic.fsd.repository.MestoRepository;
import com.katarina.stojkovic.fsd.service.KlubService;
import com.katarina.stojkovic.fsd.service.MestoService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Data
@Service
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public  class KlubServiceImpl implements KlubService {
	private final KlubRepository klubRepository;

	@Override
	public List<Klub> findAll() {
		return klubRepository.findAll();
	}

	@Override
	public Klub findById(Long klubId) {
		return klubRepository.findById(klubId)
				.orElseThrow(() -> new NoSuchElementException("KlubService.notFound"));
	}

	@Override
	public Klub save(Klub klub) {
		return klubRepository.save(klub);
	}

	@Override
	public Klub update(Klub klub) {
		return klubRepository.save(klub);
	}

	@Override
	public void deleteById(Long klubId) {
		klubRepository.deleteById(klubId);
	}


}