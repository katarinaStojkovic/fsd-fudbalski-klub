package com.katarina.stojkovic.fsd.service;



import com.katarina.stojkovic.fsd.entity.Igrac;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public  interface IgracService {

	List<Igrac> findAll();

	Igrac save(Igrac igrac, MultipartFile file) throws IOException;

	Igrac update(Igrac igrac);

	Igrac findById(Long igracId);

	void deleteById(Long igracId);

}