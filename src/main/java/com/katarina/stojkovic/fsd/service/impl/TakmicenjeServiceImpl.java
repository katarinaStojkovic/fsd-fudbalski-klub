package com.katarina.stojkovic.fsd.service.impl;

import com.katarina.stojkovic.fsd.entity.Takmicenje;
import com.katarina.stojkovic.fsd.repository.TakmicenjeRepository;
import com.katarina.stojkovic.fsd.service.TakmicenjeService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Data
@Service
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class TakmicenjeServiceImpl implements TakmicenjeService {

    private final TakmicenjeRepository takmicenjeRepository;

    @Override
    public List<Takmicenje> findAll() {
        return takmicenjeRepository.findAll();
    }

    @Override
    public Takmicenje save(Takmicenje takmicenje) {
        return takmicenjeRepository.save(takmicenje);
    }

    @Override
    public Takmicenje update(Takmicenje takmicenje) {
        return takmicenjeRepository.save(takmicenje);
    }

    @Override
    public Takmicenje findById(Long takmicenjeId) {
        return takmicenjeRepository.findById(takmicenjeId)
                .orElseThrow(() -> new NoSuchElementException("TakmicenjeService.notFound"));
    }

    @Override
    public void deleteById(Long takmicenjeId) {
        takmicenjeRepository.deleteById(takmicenjeId);
    }
}
