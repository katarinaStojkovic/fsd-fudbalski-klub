package com.katarina.stojkovic.fsd.service;




import com.katarina.stojkovic.fsd.entity.Mesto;

import java.util.List;

public  interface MestoService {

	List<Mesto> findAll();

	Mesto save(Mesto mesto);

	Mesto update(Mesto mesto);

	Mesto findById(Long mestoId);

	void deleteById(Long mestoId);

}