package com.katarina.stojkovic.fsd.service;


import com.katarina.stojkovic.fsd.entity.Takmicenje;

import java.util.List;

public  interface TakmicenjeService {

	List<Takmicenje> findAll();

	Takmicenje save(Takmicenje takmicenje);

	Takmicenje update(Takmicenje takmicenje);

	Takmicenje findById(Long takmicenjeId);

	void deleteById(Long takmicenjeId);

}