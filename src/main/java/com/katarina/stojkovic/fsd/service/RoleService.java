package com.katarina.stojkovic.fsd.service;



import com.katarina.stojkovic.fsd.entity.Role;

import java.util.List;

public  interface RoleService {

	List<Role> findAll();

	Role save(Role role);

	Role update(Role role);

	Role findById(Long roleId);

	void deleteById(Long roleId);

}