package com.katarina.stojkovic.fsd.service.impl;

import com.katarina.stojkovic.fsd.entity.Igrac;
import com.katarina.stojkovic.fsd.repository.IgracRepository;
import com.katarina.stojkovic.fsd.service.IgracService;
import com.katarina.stojkovic.fsd.util.ImageUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

@Data
@Service
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class IgracServiceImpl implements IgracService {
    private final IgracRepository igracRepository;

    @Override
    public List<Igrac> findAll() {
        return igracRepository.findAll();
    }

    @Override
    public Igrac save(Igrac igrac, MultipartFile file) throws IOException {
        igrac.setSlikaByte(ImageUtil.compressBytes(file.getBytes()));
        return igracRepository.save(igrac);
    }

    @Override
    public Igrac update(Igrac igrac) {
        return igracRepository.save(igrac);
    }

    @Override
    public Igrac findById(Long igracId) {
        return igracRepository.findById(igracId)
                .orElseThrow(() -> new NoSuchElementException("IgracService.notFound"));
    }

    @Override
    public void deleteById(Long igracId) {
        igracRepository.deleteById(igracId);
    }
}
