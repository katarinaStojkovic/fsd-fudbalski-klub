package com.katarina.stojkovic.fsd.service;

import com.katarina.stojkovic.fsd.entity.Utakmica;

import java.util.List;

public  interface UtakmicaService {

	List<Utakmica> findAll();

	Utakmica save(Utakmica utakmica);

	Utakmica update(Utakmica utakmica);

	Utakmica findById(Long utakmicaId);

	void deleteById(Long utakmicaId);

}