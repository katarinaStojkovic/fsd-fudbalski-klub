package com.katarina.stojkovic.fsd.service;


import com.katarina.stojkovic.fsd.entity.Klub;

import java.util.List;

public  interface KlubService {

	List<Klub> findAll();

	Klub save(Klub klub);

	Klub update(Klub klub);

	Klub findById(Long klubId);

	void deleteById(Long klubId);

}