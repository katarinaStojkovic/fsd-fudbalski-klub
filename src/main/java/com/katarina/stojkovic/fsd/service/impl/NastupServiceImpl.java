package com.katarina.stojkovic.fsd.service.impl;

import com.katarina.stojkovic.fsd.entity.Nastup;
import com.katarina.stojkovic.fsd.repository.IgracRepository;
import com.katarina.stojkovic.fsd.repository.NastupRepository;
import com.katarina.stojkovic.fsd.service.IgracService;
import com.katarina.stojkovic.fsd.service.NastupService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Data
@Service
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class NastupServiceImpl implements NastupService {
    private final NastupRepository nastupRepository;
    private final IgracService igracService;

    @Override
    public List<Nastup> findAll() {
        return nastupRepository.findAll();
    }

    @Override
    public Nastup save(Nastup nastup) {
        return nastupRepository.save(nastup);
    }

    @Override
    public Nastup update(Nastup nastup) {
        return nastupRepository.save(nastup);
    }

    @Override
    public Nastup findById(Long nastupId) {
      	return nastupRepository.findById(nastupId)
                .orElseThrow(() -> new NoSuchElementException("NastupService.notFound"));
    }

    @Override
    public void deleteById(Long nastupId) {
        nastupRepository.deleteById(nastupId);
    }

    @Override
    public List<Nastup> findNastupByIgracId(Long igracId) {
        return nastupRepository.findAllByIgracId(igracService.findById(igracId));
    }
}
