package com.katarina.stojkovic.fsd.service.impl;

import com.katarina.stojkovic.fsd.entity.Utakmica;
import com.katarina.stojkovic.fsd.repository.UtakmicaRepository;
import com.katarina.stojkovic.fsd.service.UtakmicaService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class UtakmicaServiceImpl implements UtakmicaService {
    private final UtakmicaRepository utakmicaRepository;

    @Override
    public List<Utakmica> findAll() {
        return utakmicaRepository.findAll();
    }

    @Override
    public Utakmica save(Utakmica utakmica) {
        return utakmicaRepository.save(utakmica);
    }

    @Override
    public Utakmica update(Utakmica utakmica) {
        return utakmicaRepository.save(utakmica);
    }

    @Override
    public Utakmica findById(Long utakmicaId) {
        return utakmicaRepository.findById(utakmicaId)
                .orElseThrow(() -> new NoSuchElementException("UtakmicaService.notFound"));
    }

    @Override
    public void deleteById(Long utakmicaId) {
        utakmicaRepository.deleteById(utakmicaId);
    }
}
