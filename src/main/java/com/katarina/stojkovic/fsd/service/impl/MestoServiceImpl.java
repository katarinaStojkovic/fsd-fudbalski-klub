package com.katarina.stojkovic.fsd.service.impl;


import com.katarina.stojkovic.fsd.entity.Mesto;
import com.katarina.stojkovic.fsd.repository.MestoRepository;
import com.katarina.stojkovic.fsd.service.MestoService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Data
@Service
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public  class MestoServiceImpl implements MestoService {
	private final MestoRepository mestoRepository;

	@Override
	public List<Mesto> findAll() {
		return mestoRepository.findAll();
	}

	@Override
	public Mesto findById(Long mestoId) {
		return mestoRepository.findById(mestoId)
				.orElseThrow(() -> new NoSuchElementException("MestoService.notFound"));
	}

	@Override
	public Mesto save(Mesto mesto) {
		return mestoRepository.save(mesto);
	}

	@Override
	public Mesto update(Mesto mesto) {
		return mestoRepository.save(mesto);
	}

	@Override
	public void deleteById(Long mestoId) {
		mestoRepository.deleteById(mestoId);
	}


}