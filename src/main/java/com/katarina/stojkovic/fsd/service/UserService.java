package com.katarina.stojkovic.fsd.service;

import com.katarina.stojkovic.fsd.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface UserService {

    User findByUsername(String username);

	List<User> findAll();

	User save(User user);

	User update(User user);

	User findById(Long userId);

	void deleteById(Long userId);
}
