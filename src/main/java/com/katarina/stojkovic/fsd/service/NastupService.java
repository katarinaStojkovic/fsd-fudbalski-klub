package com.katarina.stojkovic.fsd.service;


import com.katarina.stojkovic.fsd.entity.Nastup;

import java.util.List;

public  interface NastupService {

	List<Nastup> findAll();

	Nastup save(Nastup nastup);

	Nastup update(Nastup nastup);

	Nastup findById(Long nastupId);

	void deleteById(Long nastupId);

	List<Nastup> findNastupByIgracId(Long igracId);

}