package com.katarina.stojkovic.fsd.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "klub")
public class Klub extends Auditable{
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "naziv")
    private String naziv;
    @JoinColumn(name = "mesto_id", referencedColumnName = "id")
    @ManyToOne
    private Mesto mesto;
}
