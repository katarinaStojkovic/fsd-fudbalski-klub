package com.katarina.stojkovic.fsd.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.katarina.stojkovic.fsd.mapper.CustomLocalDateDeserializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@Table(name = "utakmica")
public class Utakmica extends Auditable{
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "datum_odigravanja")
    @JsonDeserialize(using = CustomLocalDateDeserializer.class)
    private Date datumOdigravanja;
    @JoinColumn(name = "domacin_id", referencedColumnName = "id")
    @ManyToOne
    private Klub domacinId;
    @JoinColumn(name = "gost_id", referencedColumnName = "id")
    @ManyToOne
    private Klub gostId;
    @JoinColumn(name = "takmicenje_id", referencedColumnName = "id")
    @ManyToOne
    private Takmicenje takmicenjeId;

}
