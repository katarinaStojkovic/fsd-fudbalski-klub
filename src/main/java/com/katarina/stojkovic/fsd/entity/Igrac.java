package com.katarina.stojkovic.fsd.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.katarina.stojkovic.fsd.entity.domain.Pozicija;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@Table(name = "igrac")
public class Igrac extends Auditable{
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "ime")
    private String ime;
    @Column(name = "prezime")
    private String prezime;
    @Column(name = "jmbg")
    private String jmbg;
    @Column(name = "pozicija")
    @Enumerated(EnumType.STRING)
    private Pozicija pozicija;
    @Column(name = "datum_rodj")
    private Date datumRodj;
    @Column(name = "slika_byte", length = 255)
    private byte[] slikaByte;
    @JoinColumn(name = "mesto_id", referencedColumnName = "id")
    @ManyToOne
    private Mesto mesto;
    @JoinColumn(name = "klub_id", referencedColumnName = "id")
    @ManyToOne
    private Klub klub;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    @JsonIgnore
    private User user;

}
