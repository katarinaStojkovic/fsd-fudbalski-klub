package com.katarina.stojkovic.fsd.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "mesto")
public class Mesto extends Auditable {
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "ptt")
    private Integer ptt;
    @Column(name = "naziv")
    private String naziv;

}
