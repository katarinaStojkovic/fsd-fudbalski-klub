package com.katarina.stojkovic.fsd.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
@Data
@Entity
@Table(name = "nastup")
public class Nastup extends Auditable {
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "ocena_igraca")
    private double ocenaIgraca;
    @JoinColumn(name = "utakmica_id", referencedColumnName = "id")
    @ManyToOne
    private Utakmica utakmicaId;
    @JoinColumn(name = "igrac_id", referencedColumnName = "id")
    @ManyToOne
    private Igrac igracId;

}
