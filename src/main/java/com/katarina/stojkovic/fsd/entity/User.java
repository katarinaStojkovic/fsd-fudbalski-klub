package com.katarina.stojkovic.fsd.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.List;

@Data
@Entity
@Table(name = "user")
public class User extends Auditable implements UserDetails {
    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "username")
    @NotBlank(message = "username is mandatory")
    private String username;
    @Column(name = "password")
    private String password;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    @JsonIgnore
    @ToString.Exclude
    private List<Role> roles;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return getRecordStatus() != 1;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return getRecordStatus() != 1;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return getRecordStatus() != 1;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return getRecordStatus() == 1;
    }

}
