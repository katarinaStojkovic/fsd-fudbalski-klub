package com.katarina.stojkovic.fsd.controller;

import com.katarina.stojkovic.fsd.entity.Mesto;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.service.MestoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mestos")
@RequiredArgsConstructor
public class MestoController {
    private final MestoService mestoService;


    @GetMapping
    public ResponseEntity<List<Mesto>> getAll() {
        return ResponseEntity.ok(mestoService.findAll());
    }

    @GetMapping("/{mestoId}")
    public ResponseEntity<Mesto> getById(@PathVariable Long mestoId) {
        return ResponseEntity.ok(mestoService.findById(mestoId));
    }


    @PostMapping
    public ResponseEntity<Mesto> save(@RequestBody Mesto mesto) {
        return ResponseEntity.status(201).body(mestoService.save(mesto));
    }


    @PutMapping
    public ResponseEntity<Mesto> update(@RequestBody Mesto mesto) {
        return ResponseEntity.ok(mestoService.update(mesto));
    }


    @DeleteMapping("/{mestoId}")
    public void deleteById(@PathVariable Long mestoId) {
        mestoService.deleteById(mestoId);
    }
}