package com.katarina.stojkovic.fsd.controller;


import com.katarina.stojkovic.fsd.entity.Igrac;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.service.IgracService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/igracs")
@RequiredArgsConstructor
public class IgracController {
    private final IgracService igracService;

    @AllowAdmin
    @GetMapping
    public ResponseEntity<List<Igrac>> getAll() {
        return ResponseEntity.ok(igracService.findAll());
    }

    @AllowAdmin
    @GetMapping("/{igracId}")
    public ResponseEntity<Igrac> getById(@PathVariable Long igracId) {
        return ResponseEntity.ok(igracService.findById(igracId));
    }

    @AllowAdmin
    @PostMapping
    public ResponseEntity<Igrac> save(@RequestBody Igrac igrac,@RequestParam("imageFile") MultipartFile file) throws IOException {
        return ResponseEntity.status(201).body(igracService.save(igrac,file));
    }

    @AllowAdmin
    @PutMapping
    public ResponseEntity<Igrac> update(@RequestBody Igrac igrac) {
        return ResponseEntity.ok(igracService.update(igrac));
    }

    @AllowAdmin
    @DeleteMapping("/{igracId}")
    public void deleteById(@PathVariable Long igracId) {
        igracService.deleteById(igracId);
    }
}