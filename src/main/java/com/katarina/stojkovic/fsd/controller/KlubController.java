package com.katarina.stojkovic.fsd.controller;


import com.katarina.stojkovic.fsd.entity.Klub;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.service.KlubService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/klubs")
@RequiredArgsConstructor
public class KlubController {
    private final KlubService klubService;


    @GetMapping
    public ResponseEntity<List<Klub>> getAll() {
        return ResponseEntity.ok(klubService.findAll());
    }


    @GetMapping("/{klubId}")
    public ResponseEntity<Klub> getById(@PathVariable Long klubId) {
        return ResponseEntity.ok(klubService.findById(klubId));
    }


    @PostMapping
    public ResponseEntity<Klub> save(@RequestBody Klub klub) {
        return ResponseEntity.status(201).body(klubService.save(klub));
    }


    @PutMapping
    public ResponseEntity<Klub> update(@RequestBody Klub klub) {
        return ResponseEntity.ok(klubService.update(klub));
    }


    @DeleteMapping("/{klubId}")
    public void deleteById(@PathVariable Long klubId) {
        klubService.deleteById(klubId);
    }
}