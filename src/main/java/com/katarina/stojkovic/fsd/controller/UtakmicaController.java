package com.katarina.stojkovic.fsd.controller;


import com.katarina.stojkovic.fsd.entity.Utakmica;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.service.UtakmicaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/utakmicas")
@RequiredArgsConstructor
public class UtakmicaController {
    private final UtakmicaService utakmicaService;


    @GetMapping
    public ResponseEntity<List<Utakmica>> getAll() {
        return ResponseEntity.ok(utakmicaService.findAll());
    }


    @GetMapping("/{utakmicaId}")
    public ResponseEntity<Utakmica> getById(@PathVariable Long utakmicaId) {
        return ResponseEntity.ok(utakmicaService.findById(utakmicaId));
    }


    @PostMapping
    public ResponseEntity<Utakmica> save(@RequestBody Utakmica utakmica) {
        return ResponseEntity.status(201).body(utakmicaService.save(utakmica));
    }


    @PutMapping
    public ResponseEntity<Utakmica> update(@RequestBody Utakmica utakmica) {
        return ResponseEntity.ok(utakmicaService.update(utakmica));
    }

    @DeleteMapping("/{utakmicaId}")
    public void deleteById(@PathVariable Long utakmicaId) {
        utakmicaService.deleteById(utakmicaId);
    }
}