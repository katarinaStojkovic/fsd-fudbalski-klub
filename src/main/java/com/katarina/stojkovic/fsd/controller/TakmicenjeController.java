package com.katarina.stojkovic.fsd.controller;


import com.katarina.stojkovic.fsd.entity.Takmicenje;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.service.TakmicenjeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/takmicenjes")
@RequiredArgsConstructor
public class TakmicenjeController {
    private final TakmicenjeService takmicenjeService;

    @GetMapping
    public ResponseEntity<List<Takmicenje>> getAll() {
        return ResponseEntity.ok(takmicenjeService.findAll());
    }

    @GetMapping("/{takmicenjeId}")
    public ResponseEntity<Takmicenje> getById(@PathVariable Long takmicenjeId) {
        return ResponseEntity.ok(takmicenjeService.findById(takmicenjeId));
    }


    @PostMapping
    public ResponseEntity<Takmicenje> save(@RequestBody Takmicenje takmicenje) {
        return ResponseEntity.status(201).body(takmicenjeService.save(takmicenje));
    }

    @PutMapping
    public ResponseEntity<Takmicenje> update(@RequestBody Takmicenje takmicenje) {
        return ResponseEntity.ok(takmicenjeService.update(takmicenje));
    }

    @DeleteMapping("/{takmicenjeId}")
    public void deleteById(@PathVariable Long mestoId) {
        takmicenjeService.deleteById(mestoId);
    }
}