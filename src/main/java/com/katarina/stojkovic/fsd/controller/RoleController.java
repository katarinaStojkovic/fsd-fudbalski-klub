package com.katarina.stojkovic.fsd.controller;

import com.katarina.stojkovic.fsd.entity.Role;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/roles")
@RequiredArgsConstructor
public class RoleController {
	private final RoleService roleService;

	@AllowAdmin
	@GetMapping
	public ResponseEntity<List<Role>> getAll() {
		return ResponseEntity.ok(roleService.findAll());
	}

	@AllowAdmin
	@GetMapping("/{roleId}")
	public ResponseEntity<Role> getById(@PathVariable Long roleId) {
		return ResponseEntity.ok(roleService.findById(roleId));
	}

	@AllowAdmin
	@PostMapping
	public ResponseEntity<Role> save(@RequestBody Role role) {
		return ResponseEntity.status(201).body(roleService.save(role));
	}

	@AllowAdmin
	@PutMapping
	public ResponseEntity<Role> update(@RequestBody Role role) {
		return ResponseEntity.ok(roleService.update(role));
	}

	@AllowAdmin
	@DeleteMapping("/{roleId}")
	public void deleteById(@PathVariable Long roleId) {
		roleService.deleteById(roleId);
	}

}

