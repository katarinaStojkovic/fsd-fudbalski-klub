package com.katarina.stojkovic.fsd.controller;


import com.katarina.stojkovic.fsd.entity.Nastup;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowIgrac;
import com.katarina.stojkovic.fsd.service.NastupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nastups")
@RequiredArgsConstructor
public class NastupController {
    private final NastupService nastupService;

    @AllowAdmin
    @GetMapping
    public ResponseEntity<List<Nastup>> getAll() {
        return ResponseEntity.ok(nastupService.findAll());
    }

    @AllowIgrac
    @GetMapping("/{igracId}")
    public ResponseEntity<List<Nastup>> getNastupByIgracId(@PathVariable Long igracId){
        return ResponseEntity.ok(nastupService.findNastupByIgracId(igracId));
    }

    @AllowAdmin
    @GetMapping("/{nastupId}")
    public ResponseEntity<Nastup> getById(@PathVariable Long nastupId) {
        return ResponseEntity.ok(nastupService.findById(nastupId));
    }

    @AllowAdmin
    @PostMapping
    public ResponseEntity<Nastup> save(@RequestBody Nastup nastup) {
        return ResponseEntity.status(201).body(nastupService.save(nastup));
    }

    @AllowAdmin
    @PutMapping
    public ResponseEntity<Nastup> update(@RequestBody Nastup nastup) {
        return ResponseEntity.ok(nastupService.update(nastup));
    }

    @AllowAdmin
    @DeleteMapping("/{nastupId}")
    public void deleteById(@PathVariable Long nastupId) {
        nastupService.deleteById(nastupId);
    }
}