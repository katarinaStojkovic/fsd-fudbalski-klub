package com.katarina.stojkovic.fsd.controller;

import com.katarina.stojkovic.fsd.entity.User;
import com.katarina.stojkovic.fsd.security.annotation.role.AllowAdmin;
import com.katarina.stojkovic.fsd.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
	private final UserService userService;

	@AllowAdmin
	@GetMapping
	public ResponseEntity<List<User>> getAll() {
		return ResponseEntity.ok(userService.findAll());
	}

	@AllowAdmin
	@GetMapping("/{userId}")
	public ResponseEntity<User> getById(@PathVariable Long userId) {
		return ResponseEntity.ok(userService.findById(userId));
	}

	@AllowAdmin
	@PostMapping
	public ResponseEntity<User> save(@RequestBody User user) {
		return ResponseEntity.status(201).body(userService.save(user));
	}

	@AllowAdmin
	@PutMapping
	public ResponseEntity<User> update(@RequestBody User user) {
		return ResponseEntity.ok(userService.update(user));
	}

	@AllowAdmin
	@DeleteMapping("/{userId}")
	public void deleteById(@PathVariable Long userId) {
		userService.deleteById(userId);
	}

}

