package com.katarina.stojkovic.fsd.repository;

import com.katarina.stojkovic.fsd.entity.Igrac;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IgracRepository  extends JpaRepository<Igrac, Long> {
}
