package com.katarina.stojkovic.fsd.repository;

import com.katarina.stojkovic.fsd.entity.Klub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KlubRepository extends JpaRepository<Klub, Long> {
}
