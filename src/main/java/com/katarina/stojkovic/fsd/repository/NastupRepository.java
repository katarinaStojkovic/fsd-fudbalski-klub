package com.katarina.stojkovic.fsd.repository;

import com.katarina.stojkovic.fsd.entity.Igrac;
import com.katarina.stojkovic.fsd.entity.Nastup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NastupRepository extends JpaRepository<Nastup, Long> {
    List<Nastup> findAllByIgracId(Igrac igrac);
}