package com.katarina.stojkovic.fsd.repository;

import com.katarina.stojkovic.fsd.entity.Utakmica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtakmicaRepository extends JpaRepository<Utakmica, Long> {

}