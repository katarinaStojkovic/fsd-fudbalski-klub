package com.katarina.stojkovic.fsd.repository;

import com.katarina.stojkovic.fsd.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public  interface RoleRepository extends JpaRepository<Role, Long> {

}
