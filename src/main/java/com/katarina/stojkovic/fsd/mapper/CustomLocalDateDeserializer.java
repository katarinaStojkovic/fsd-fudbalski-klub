package com.katarina.stojkovic.fsd.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDate;

public class CustomLocalDateDeserializer extends JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonToken token = jsonParser.getCurrentToken();
        if (token == JsonToken.VALUE_STRING) {
            String dateString = jsonParser.getText().trim();
            if (dateString.length() > 10) {
                return LocalDate.parse(dateString.substring(0, 10)).plusDays(1);
            }
            return LocalDate.parse(dateString);
        }
        throw deserializationContext.instantiationException(LocalDate.class, "Not string");
    }
}
